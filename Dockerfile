FROM php:7.4-fpm
RUN if [ ! -d ./app  ]; then mkdir ./app; fi
COPY ./app /var/www/html

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY --from=alpine/git:latest /git /git

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN apt-get update && apt-get install -y git

RUN cd /var/www/html &&  composer update